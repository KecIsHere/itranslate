package itranslate.kec.com.myapplication

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.Window
import android.view.WindowManager
import android.widget.TextView




abstract class BasicActivity(@LayoutRes private var layoutResourceId: Int, private var fullScreen: Boolean) : AppCompatActivity() {

    private var fragmentManager: FragmentManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(fullScreen) {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }

        setContentView(layoutResourceId)


        val toolbar: Toolbar? = findViewById(R.id.toolbar)
        if (toolbar != null) {
            setSupportActionBar(toolbar)
            supportActionBar!!.title = ""
        }

        fragmentManager = supportFragmentManager
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        return true
    }

    fun setToolbarTitle(title : String) {
        val toolbar: Toolbar? = findViewById(R.id.toolbar)

        val mTitle = toolbar!!.findViewById(R.id.toolbarTitle) as TextView
        mTitle.text = title
    }
}