package itranslate.kec.com.myapplication

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import itranslate.kec.com.myapplication.audio.AudioListActivity
import itranslate.kec.com.myapplication.utils.Constants.Companion.getNotAllowedPermissions
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.IOException
import itranslate.kec.com.myapplication.services.recordService


class MainActivity : BasicActivity(R.layout.activity_main, true) {

    private val MY_PERMISSIONS_REQUEST : Int = 100
    private var isRecording : Boolean = false
    private var recorder: MediaRecorder? = null
    private var audioFile : File? = null
    private var mStartTime : Long = 0
    private val timeCounterHandler : Handler = Handler() // Time counter
    private var goToList : Boolean = false //Go to list or start recording

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnRecord.setOnClickListener {
            goToList = false
            checkPermission()
        }

        btnShowRecordings.setOnClickListener {
            goToList = true
            checkPermission()
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
        timeCounterHandler.removeCallbacks(mUpdateTimeTask)
    }

    private fun startRecording() {

        //TODO creating file could go to separated method
        audioFile = recordService.createPublicFile()

        if(audioFile == null) {
            Toast.makeText(this, getString(R.string.creating_file_error), Toast.LENGTH_SHORT).show()
            return
        }

        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(audioFile!!.absolutePath)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                prepare()
            } catch (e: IOException) {
                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
            }

            start()

            mStartTime = System.currentTimeMillis()
            timeCounterHandler.postDelayed(mUpdateTimeTask, 0)
        }

    }

    private val mUpdateTimeTask = object : Runnable {
        override fun run() {
            val start = mStartTime
            val millis = System.currentTimeMillis() - start
            val seconds = (millis / 1000).toInt()
            val minutes = seconds / 60

            if (seconds < 10) {
                txtTime.setText(minutes.toString() + ":0" + seconds % 60)
            } else {
                txtTime.setText(minutes.toString() + ":" + seconds % 60)
            }

            timeCounterHandler.postDelayed(this, 1000)
        }
    }

    private fun checkPermission() {
        val neededPermissions =
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO)
        val notAllowedPermissions = getNotAllowedPermissions(this, neededPermissions)
        if (notAllowedPermissions.isEmpty()) {
            if (goToList) {
                startActivity(Intent(this, AudioListActivity::class.java))
            }
            else {
                if(isRecording) {
                    isRecording = !isRecording
                    stopRecording()
                }
                else {
                    isRecording = !isRecording
                    startRecording()
                }
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                notAllowedPermissions,
                MY_PERMISSIONS_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted
                    if(isRecording) {
                        isRecording = !isRecording
                        stopRecording()
                    }
                    else {
                        isRecording = !isRecording
                        startRecording()
                    }
                } else {
                    // permission denied
                    for (permission in permissions) {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                permission)) {
                            showPermissionExplanationDialog()
                            return
                        }
                    }
                }
                return
            }

            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun showPermissionExplanationDialog() {
        val dialog = Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog.setContentView(R.layout.permission_explanation_dialog)
        val dialogButtonAllow = dialog.findViewById(R.id.btnAllow) as Button
        // if button is clicked, close the custom dialog
        dialogButtonAllow.setOnClickListener {
            dialog.dismiss()
            checkPermission()
        }

        val dialogTxtLater = dialog.findViewById(R.id.txtLater) as TextView

        dialogTxtLater.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
