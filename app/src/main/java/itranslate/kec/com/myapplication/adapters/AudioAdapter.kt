package itranslate.kec.com.myapplication.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import itranslate.kec.com.myapplication.R
import itranslate.kec.com.myapplication.model.Model
import kotlinx.android.synthetic.main.audio_item.view.*
import android.view.animation.Animation
import android.view.animation.Transformation
import android.app.Activity
import android.util.DisplayMetrics




class AudioAdapter(private val context: Context,
                   private val items: MutableList<Model.Record>,
                   private val clickListener: (Model.Record) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun getItemCount() = items.count()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AudioViewHolder).bind(items[position], clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AudioViewHolder(LayoutInflater.from(context).inflate(R.layout.audio_item, parent, false), context)
    }

    fun removeAt(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    class AudioViewHolder(itemView: View, private val context: Context) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Model.Record, clickListener: (Model.Record) -> Unit) {
            itemView.txtTitle.text = item.name

            val seconds = (item.duration / 1000).toInt()
            val minutes = seconds / 60

            if (seconds < 10) {
                itemView.txtTime.setText(minutes.toString() + ":0" + seconds % 60)
            } else {
                itemView.txtTime.setText(minutes.toString() + ":" + seconds % 60)
            }
            itemView.mainContainer.setOnClickListener{
                val displayMetrics = DisplayMetrics()
                (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
                val resizeAnimation = ResizeAnimation(
                    itemView.playingView,
                    displayMetrics.widthPixels
                )
                resizeAnimation.duration = item.duration
                resizeAnimation.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation){

                    }

                    override fun onAnimationEnd(animation: Animation){
                        val resizeAnimationEnd = ResizeAnimation(
                            itemView.playingView,
                            1
                        )
                        resizeAnimationEnd.duration = 1
                        itemView.playingView.startAnimation(resizeAnimationEnd)
//                        thread {
//                            Thread.sleep(10)
//                            context.runOnUiThread(
//                                Runnable {
//                                    itemView.playingView.layoutParams.width = 1
//                                    itemView.playingView.requestLayout()
//                                }//public void run() {
//                            )
//                        }
                    }

                    override fun onAnimationRepeat(animation: Animation) {

                    }

                })


                itemView.playingView.startAnimation(resizeAnimation)

                clickListener(item)
            }
        }
    }

    class ResizeAnimation(
        private var view: View,
        private val targetHeight: Int
    ) : Animation() {

        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            val newHeight = (targetHeight * interpolatedTime).toInt()
            //to support decent animation, change new heigt as Nico S. recommended in comments
            //int newHeight = (int) (startHeight+(targetHeight - startHeight) * interpolatedTime);
            view.layoutParams.width = newHeight
            view.requestLayout()
        }

        override fun initialize(width: Int, height: Int, parentWidth: Int, parentHeight: Int) {
            super.initialize(width, height, parentWidth, parentHeight)
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }
}