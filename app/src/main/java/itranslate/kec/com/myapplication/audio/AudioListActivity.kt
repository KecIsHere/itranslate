package itranslate.kec.com.myapplication.audio

import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import itranslate.kec.com.myapplication.BasicActivity
import itranslate.kec.com.myapplication.R
import itranslate.kec.com.myapplication.adapters.AudioAdapter
import itranslate.kec.com.myapplication.adapters.SwipeToDeleteCallback
import itranslate.kec.com.myapplication.model.Model
import itranslate.kec.com.myapplication.services.recordService
import kotlinx.android.synthetic.main.activity_audio_list.*
import java.io.IOException

class AudioListActivity : BasicActivity(R.layout.activity_audio_list, false) {

    var records : MutableList<Model.Record>? = null
    var player: MediaPlayer? = null
    var currentPlaying: Model.Record? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setToolbarTitle(getString(R.string.recordings))

        val records : MutableList<Model.Record>? = recordService.getRecords(this) //Get records from storage

        recyclerRecords.layoutManager = LinearLayoutManager(this)
        recyclerRecords.adapter = AudioAdapter(this, records!!) { value: Model.Record -> setOnValueClick(value) }

        val swipeHandler = object : SwipeToDeleteCallback(this) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val adapter = recyclerRecords.adapter as AudioAdapter
                recordService.deleteRecord(records[viewHolder.adapterPosition])
                adapter.removeAt(viewHolder.adapterPosition)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerRecords)
    }

    private fun setOnValueClick(value: Model.Record){
        if(player == null) {
            startPlaying(value)
        }
        else {
            startPlaying(value)
        }
    }

    private fun startPlaying(record: Model.Record) {
        player = MediaPlayer().apply {
            try {
                setDataSource(record.url)
                prepare()
                start()
            } catch (e: IOException) {
                Toast.makeText(applicationContext, e.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun stopPlaying(record: Model.Record) {
        player?.release()
        player = null
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_list, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {

            R.id.actionDone -> {
                onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
