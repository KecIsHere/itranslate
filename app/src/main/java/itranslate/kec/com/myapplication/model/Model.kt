package itranslate.kec.com.myapplication.model

object Model {

    data class Record(
        val name: String,
        val url: String,
        val duration: Long,
        val startTimestamp: Long,
        val index: Int
    )
}