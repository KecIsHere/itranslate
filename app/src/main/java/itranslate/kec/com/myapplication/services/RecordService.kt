package itranslate.kec.com.myapplication.services

import android.content.Context
import android.os.Environment
import itranslate.kec.com.myapplication.model.Model
import itranslate.kec.com.myapplication.utils.Constants
import java.io.File
import android.media.MediaMetadataRetriever

class RecordService(context: Context) {
    companion object {
        private var instance: RecordService? = null

        @Synchronized
        fun getInstance(ctx: Context): RecordService {
            if (instance == null) {
                instance = RecordService(ctx.applicationContext)
            }
            return instance!!
        }
    }

    fun createPublicFile(): File? {
        // Get the directory for the user's public pictures directory.
        val directoryFile = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), Constants.ALBUM_NAME
        )
        if(!directoryFile.exists()) {
            if (!directoryFile.mkdirs()) {
                return null
            }
        }

        val tsLong = System.currentTimeMillis() / 1000
        return File(directoryFile, tsLong.toString() + ".3gp")
    }

    fun getRecords(context: Context): MutableList<Model.Record>? {
        // Get the directory for the user's public pictures directory.
        val directoryFile = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), Constants.ALBUM_NAME
        )
        if(!directoryFile.exists()) {
            return null
        }

        val files = directoryFile.listFiles()
        val recordsList: MutableList<Model.Record> = mutableListOf()
        for (i in files.indices) {
            val recordFile : File = files[i]
            val mmr = MediaMetadataRetriever()
            mmr.setDataSource(recordFile.absolutePath)
            recordsList.add(Model.Record("Recording " + (i+1).toString(), recordFile.absolutePath, mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong(), 0, i))
        }

        return recordsList
    }



    fun deleteRecord(record: Model.Record) {
        // Get the directory for the user's public pictures directory.
        val recordFile = File(record.url)
        if(recordFile.exists()) {
            recordFile.delete()
        }

    }
}

val Context.recordService: RecordService
    get() = RecordService.getInstance(applicationContext)