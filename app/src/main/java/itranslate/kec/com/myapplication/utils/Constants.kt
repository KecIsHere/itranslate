package itranslate.kec.com.myapplication.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat

class Constants {
    companion object {
        const val ALBUM_NAME = "iTranslate"

        fun getNotAllowedPermissions(context: Context, permissions: Array<String>): Array<String> {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val notAllowedPermissionsArrayList: ArrayList<String> = ArrayList()
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        notAllowedPermissionsArrayList.add(permission)
                    }
                }
                return notAllowedPermissionsArrayList.toTypedArray()
            }

            return arrayOf()
        }
    }
}