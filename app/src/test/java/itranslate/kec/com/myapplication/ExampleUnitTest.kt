package itranslate.kec.com.myapplication

import android.content.Intent
import android.widget.Button
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowActivity
import org.robolectric.shadows.ShadowIntent
import org.hamcrest.CoreMatchers.equalTo

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(RobolectricTestRunner::class)
@Config(constants=BuildConfig::class, packageName = "itranslate.kec.com.myapplication")

class ExampleUnitTest {

    var mainActivity:MainActivity= null!!;
    @Before
    fun init(){
        mainActivity = Robolectric.setupActivity(MainActivity::class.java)
    }

    @Test
    fun checkButtonTitle(){
        val button= mainActivity.findViewById<Button>(R.id.btnShowRecordings)

        assertNotNull(button);

        assertThat(button.text.toString(),equalTo("Show Recordings"))
    }
}
